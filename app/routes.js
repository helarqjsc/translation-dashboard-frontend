import React from 'react';
import { Route, IndexRoute } from 'react-router';

import asyncComponent from './utils/asyncComponent'; /* for async page, show loading component */

import Root from './containers/Root';
import Translations from './containers/Translations';
import TranslationEdit from './containers/TranslationEdit';
import ImportPage from './containers/Import';
import ExportPage from './containers/Export';
import About from './components/About';

export default (
  <Route path="/" component={Root}>
    <IndexRoute component={Translations} />
    <Route path="/edit/:translationId" component={TranslationEdit} />
    <Route path="/import" component={ImportPage} />
    <Route path="/export" component={ExportPage} />

    { /* async component */}
    <Route path="/async-example" getComponent={(location, callback) =>
        asyncComponent(require.ensure([], require => callback('', require('./components/AsyncExample').default), 'async-example'))
    } />

    <Route path="/about" component={About} />
  </Route>
);
