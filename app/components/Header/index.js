import React from 'react';
import { Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';

import Nav from 'react-bootstrap/lib/Nav';
import Navbar from 'react-bootstrap/lib/Navbar';
import NavItem from 'react-bootstrap/lib/NavItem';

/* component styles */
import s from './styles.css';

function Header() {
  return (
    <Navbar inverse collapseOnSelect className={s.root}>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">Marco Togni's Translation Dashboard</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>

      <Navbar.Collapse>
        <Nav>
          <LinkContainer className={s.link} to="/">
            <NavItem>Translations</NavItem>
          </LinkContainer>
          <LinkContainer className={s.link} to="/import">
            <NavItem>Import</NavItem>
          </LinkContainer>
          <LinkContainer className={s.link} to="/export">
            <NavItem>Export</NavItem>
          </LinkContainer>
          {/*<NavItem>Edit Users</NavItem>*/}
          {/*<NavItem>Log out</NavItem>*/}
        </Nav>
      </Navbar.Collapse>

    </Navbar>
  );
};

export default Header;
