import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { observer } from 'mobx-react';
import connect from 'stores/connect';
import { Table, ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap/lib';


/* component styles */
import s from './styles.css';

class ExportPage extends Component {

  componentDidMount() {

  }

  render() {

    return (
      <section className={s.root}>
        <Helmet
          title="Export Translations"
        />
        <h1>Export Translations</h1>

        <textarea defaultValue={"lsssel"}></textarea>
        <Button bsStyle="success" className={s.button}>Export</Button>
      </section>
    );
  }
}

export default connect(observer(ExportPage));
