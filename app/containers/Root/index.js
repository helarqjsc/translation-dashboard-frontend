import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import R from 'ramda';

import Panel from 'react-bootstrap/lib/Panel';

import connect from 'stores/connect';
import Header from 'components/Header';
import Loading from 'components/Loading';

import s from './styles/app.css';

/* global styles for app */
if (__CLIENT__) {
  require('./styles/app.css');
}

class Root extends Component {
  static propTypes = {
    location: PropTypes.object,
    children: PropTypes.object,
    params: PropTypes.object,
    history: PropTypes.object,
    app: PropTypes.object,
  };

  componentDidMount() {
    const { app } = this.props;

    app.hideLoading();
  }

  componentDidUpdate(prevProps) {
    const { app, location } = this.props;

    if (app.isLoading === true && !R.equals(location, prevProps.location)) {
      app.hideLoading();
    }
  }

  render() {
    const { app } = this.props;

    return (
      <div>
        <section>
          <Header />
        </section>

        <section className={s.wrapper}>
          <Panel>
            {
              app.isLoading
                ? <Loading />
                : this.props.children &&
                    React.cloneElement(this.props.children, this.props)
            }
          </Panel>
        </section>

      </div>
    );
  }
}

export default connect(observer(Root));
