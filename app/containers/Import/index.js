import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { observer } from 'mobx-react';
import connect from 'stores/connect';
import { Table, ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap/lib';


/* component styles */
import s from './styles.css';

class ImportPage extends Component {

  componentDidMount() {

  }

  render() {

    return (
      <section className={s.root}>
        <Helmet
          title="Import Translations"
        />
        <h1>Import Translations</h1>

        <textarea defaultValue={"lsssel"}></textarea>
        <Button bsStyle="success" className={s.button}>Import</Button>
      </section>
    );
  }
}

export default connect(observer(ImportPage));
