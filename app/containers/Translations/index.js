import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { observer } from 'mobx-react';
import connect from 'stores/connect';
import { Table, ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap/lib';
import { Link } from 'react-router';


/* component styles */
import s from './styles.css';

class Translations extends Component {
  static propTypes = {
    translations: PropTypes.object,
    languages: PropTypes.object,
  };

  componentDidMount() {
    const { translations, languages } = this.props;

    // Get translations from api, see /app/mobx/stores/translations
    translations.getAll();
    languages.getLanguages();
  }

  render() {
    const { translations, languages } = this.props;

    return (
      <section className={s.root}>
        <Helmet
          title="translations"
        />
        <h1>Translations page</h1>
        {
          translations.isFetching &&
          <span>Loading...</span>
        }
        <Table striped bordered condensed hover className={s.table}>
          <thead>
          <tr>
            <th>#</th>
            <th>Language</th>
            <th>Element</th>
            <th>Original text</th>
            <th>Translated text</th>
            <th>Created at</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          { translations.items &&
            translations.items.map((translation) =>
              languages.itemsById[translation.language_id] &&
              <tr key={translation.id}>
                <td>{translation.id}</td>
                <td>{languages.itemsById[translation.language_id].full_name}</td>
                <td>{translation.element_name}</td>
                <td>{translation.original_text}</td>
                <td>{translation.translated_text}</td>
                <td>{translation.created_at}</td>
                <td>
                  <ButtonToolbar>
                    <Button>
                      <Link to={`/edit/${translation.id}`}>
                        <Glyphicon glyph="pencil" />
                      </Link>
                    </Button>
                    <Button><Glyphicon glyph="remove" /></Button>
                </ButtonToolbar>
              </td>
            </tr>
            )
          }

          </tbody>
        </Table>
        <Button onClick={() => translations.addItem()}>Add Item</Button>
      </section>
    );
  }
}

export default connect(observer(Translations));
