import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { observer } from 'mobx-react';
import connect from 'stores/connect';
import { Table, ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap/lib';
import {browserHistory} from 'react-router';


/* component styles */
import s from './styles.css';

class TranslationEdit extends Component {
  static propTypes = {
    translations: PropTypes.object,
    languages: PropTypes.object,
  };
  
  constructor() {
    super();
  }
  
  componentDidMount() {
    const { translations, languages, params: { translationId }} = this.props;
    const translation = translations.itemsById[translationId];
    const language = translation && languages.itemsById[translation.language_id];

    Promise.all([ translations.getAll(), languages.getLanguages()])
      .then(() => {
        const translation = translations.itemsById[translationId];
        const language = languages.itemsById[translation.language_id];
        this.setState({ translation });
        this.setState({ language });
      })
  }
  
  handleSave() {
    const { translations } = this.props;

    translations.saveItem(this.state.translation);
  }

  handleDelete() {
    const { translations } = this.props;

    if (window.confirm("Do you really want to delete this item?")) {
      translations.deleteItem(this.state.translation);
      browserHistory.push('/');
    }
  }

  handleTranslationChange (event) {
    const translation = this.state.translation;
    translation.translated_text = event.target.value;
    this.setState({ translation });
  }

  render() {
    if (!this.state) return null;
    const {translation, language} = this.state;
    if (!translation || !language ) return null;

    return (
      <section className={s.root}>
        <Helmet
          title="Edit Translation"
        />
        <h1>Edit Translation</h1>

        {translation && language &&
          <div>
            <div>
              <b>ID:</b> <span>{language.id}</span>
            </div>
            <div>
              <b>Language:</b> <span>{language.full_name}</span>
            </div>
            <div>
              <b>Element name:</b> <span>{translation.element_name}</span>
            </div>
            <div>
              <b>Created at:</b> <span>{translation.created_at}</span>
            </div>
            <Table striped bordered condensed hover className={s.table}>
              <thead>
              <tr>
                <th>Original text</th>
                <th>Translated text</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>
                  <textarea className={s.textarea}
                            disabled
                            value={translation.original_text}></textarea>
                </td>
                <td>
                  <textarea className={s.textarea}
                            value={translation.translated_text}
                            onChange={(e) => this.handleTranslationChange(e)}></textarea>
                </td>
              </tr>
              </tbody>
            </Table>
            <Button onClick={() => this.handleDelete()} className={s.button} bsStyle="danger">Delete</Button>
            <Button onClick={() => this.handleSave()} className={s.button} bsStyle="success">Save</Button>
          </div>
        }
      </section>
    );
  }
}

export default connect(observer(TranslationEdit));
