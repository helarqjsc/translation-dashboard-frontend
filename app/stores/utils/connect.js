import React, { Component } from 'react';
import { posts, app, translations, languages } from '../';

function connect(DecoratedComponent) {
  class Connect extends Component {
    render() {
      return (
        <DecoratedComponent
          translations={translations}
          languages={languages}
          posts={posts}
          app={app}
          {...this.props}
        />
      );
    }
  }

  return Connect;
}

export default connect;
