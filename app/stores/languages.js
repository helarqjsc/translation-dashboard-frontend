import { observable, action } from 'mobx';
import { apiUrl } from 'utils/config';
import axios from 'axios';

class Languages {
  @observable items = [];
  @observable itemsById = {};
  @observable isFetching = false;

  // Get items from api
  @action getLanguages() {

    if (this.items.length) {
      return this.items;
      return new Promise((resolve, reject) => {
        resolve(this.items);
      });
    }
    axios
      .get(apiUrl + 'languages')
      .then(res => {
        this.items = res.data;
        this.itemsById = this.items.reduce((acc, i) => { acc[i.id] = i; return acc}, {});
        this.isFetching = false;
        return this.items;
      });
  }

  // Add example item
  @action addItem() {
    this.items.push({
      id: this.items.length + 1,
      text: `example ${this.items.length}`,
    });
  }
}

export default new Languages();
