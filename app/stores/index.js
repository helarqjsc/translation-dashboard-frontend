import posts from './posts';
import app from './app';
import translations from './translations';
import languages from './languages';

export {
  posts,
  app,
  translations,
  languages,
};
