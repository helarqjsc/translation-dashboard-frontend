import { observable, action } from 'mobx';
import { apiUrl } from 'utils/config';
import axios from 'axios';

class Translations {
  @observable items = [];
  @observable itemsById = {};
  @observable isFetching = false;

  @action getAll() {
    this.isFetching = true;

    console.log("this.items.length", this.items.length);
    if (this.items.length) {
      return new Promise((resolve, reject) => {
        resolve(this.items);
      });
    }
    return axios
      .get(apiUrl + 'translations')
      .then(res => {
        this.items = res.data;
        this.generateItemsById();
        this.isFetching = false;
        return this.items;
      });
  }

  @action addItem() {
    this.items.push({
      id: this.items.length + 1,
      text: `example ${this.items.length}`,
    });
  }

  @action saveItem(item) {
    axios
      .put(`${apiUrl}translations/${item.id}`, item);
  }

  @action deleteItem(item) {
    axios
      .delete(`${apiUrl}translations/${item.id}`)
      .then(res => {
        this.items.remove(this.items.find(a => a.id === item.id));
        generateItemsById();
      });
  }
  //
  // toJS() {
  //   return this.items.map(i => i.toJS());
  // }

  generateItemsById() {
    this.itemsById = this.items.reduce((acc, i) => { acc[i.id] = i; return acc}, {});
  }
}

export default new Translations();
